<?php

namespace Admin\Users\Entity;

class User
{
    public $id;
    public $name;
    public $email;
    public $email_verified_at;
    public $password;
    public $remember_token;
    public $created_at;
    public $updated_at;

    public function __construct($params) {
        $this->id = isset($params['id']) ? isset($params['id']) : null;
        $this->name = isset($params['name']) ? isset($params['name']) : '';
        $this->email = isset($params['email']) ? isset($params['email']) : '';
        $this->email_verified_at = isset($params['email_verified_at']) ? isset($params['email_verified_at']) : null;
        $this->password = isset($params['password']) ? isset($params['password']) : null;
        $this->remember_token = isset($params['remember_token']) ? isset($params['remember_token']) : null;
        $this->created_at = isset($params['created_at']) ? isset($params['created_at']) : null;
        $this->updated_at = isset($params['updated_at']) ? isset($params['updated_at']) : null;
    }
}
