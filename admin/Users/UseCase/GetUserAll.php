<?php

namespace Admin\Users\UseCase;

class GetUserAll
{
    private $repository;

    public function __construct($repository) {
        $this->repository = $repository;
    }

    /**
     * Get all
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function execute() {
        return $this->repository->all();
    }
}
