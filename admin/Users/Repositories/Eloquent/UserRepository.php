<?php

namespace Admin\Users\Repositories\Eloquent;

use App\Models\User;
use Admin\Users\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
   /**
     * Get one
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function all() 
    {
        return User::select()->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id) {
        return User::select()->where('id', $id).first();
    }

    /**
     * Create
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function store($user) 
    {
        return User::create($user);
    }

    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id, $user) 
    {
        return User::update($user)->where('id', $id);
    }

    /**
     * Delete
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function delete(int $id) 
    {
        return User::where('id', $id)->delete();
    }
}
