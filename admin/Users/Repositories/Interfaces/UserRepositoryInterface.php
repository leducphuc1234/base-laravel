<?php

namespace Admin\Users\Repositories\Interfaces;

interface UserRepositoryInterface 
{
    /**
     * Get all
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function all();

    /**
     * Store a newly created resource in storage.
     * @param array $user
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function store(array $user);

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id);

    /**
     * Update
     * @param int $id
     * @param array $user
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function update(int $id, array $user);

    /**
     * Delete
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function delete(int $id);
}
