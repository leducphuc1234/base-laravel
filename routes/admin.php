<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', function () {
    return view('pages.index');
});

Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::get('/user', 'UserController@index');
});
