<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Admin\Users\Repositories\Interfaces\UserRepositoryInterface;
use Admin\Users\Repositories\Eloquent\UserRepository;

class AppServiceProvider extends ServiceProvider
{
    private $repositories = [
        UserRepositoryInterface::class => UserRepository::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        foreach($this->repositories as $key => $value) {
            $this->app->singleton($key, $value);
        }
    }
}
