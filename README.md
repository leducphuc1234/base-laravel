# Author:
* Mr.Tuan <<https://gitlab.com/tuan.nguyen.anh>>

Creator: 
* Mr.Duc Phuc <<leducphuc1234@gmail.com>>

# Description


# Technical
* [Laravel][]
* [Entrust][]

## Development

**1. Download package with composer:** 

Run terminal:

    compose install

**1. Migrate:** 

Run terminal:

    php artisan migrate

**2. Create ENV**
 - Copy file .env.example to .env

**3. Environment variable**

**4. Run laravel development**

    php artisan serve

Open http://127.0.0.1:8000/


[Laravel]: https://laravel.com/docs/5.8
[Entrust]: https://github.com/Zizaco/entrust
